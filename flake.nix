{
  description = "Build your typst documents with packages managed/pinned by nix.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    typst-bin.url = "github:typst/typst/0198429e63d288b992624b0fb615fc5af5aa6cb6";
    typst-packages = {
      url = "github:typst/packages/71c7f1eedaa29967f0a854ad38185bcea74438bb";
      flake = false;
    };
  };

  outputs = {
    nixpkgs,
    typst-packages,
    typst-bin,
    ...
  }: let
    system = "x86_64-linux";
    typst = typst-bin.packages.${system}.default.overrideAttrs {
      patches = ./folder_override.patch;
    };
  in {
    packages.${system}.default = with import nixpkgs {inherit system;};
    # TODO: Add formatter(s) (and maybe other tooling?! lsp?!)
      stdenv.mkDerivation rec {
        name = "typst-packed";
        src = ./.;

        inputName = "index";
        outputName = inputName;
        outputFormat = "pdf";

        configurePhase = ''
          # Link typst packages so the typst plugin logic picks them up
          # This makes it so packages are found under `typst/packages/preview/<name>/<version>`
          ln -s ${typst-packages} ./typst
        '';

        buildPhase = ''
          # As we just throw them in the temporary build folder
          # typst only needs to know about the fact there is a override
          PACKAGE_BASE_DIR="" ${typst}/bin/typst \
            compile \
            ./${inputName}.typ \
            ./${outputName}.${outputFormat}
        '';

        installPhase = ''
          mkdir -p $out;
          cp ./${outputName}.${outputFormat} $out/${outputName}.${outputFormat};
        '';
      };
  };
}
