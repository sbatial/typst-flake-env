# Pack your ~bags~ typst documents

This project arose out of the wish to have my [typst](https://github.com/typst/typst) packages managed and to have an easy local and CI environment.
So I grabbed my tool of choice for the job (nix flakes) and got to work.
And immediately hit a bump in the road when realizing that [typst does not support custom/non-global package paths](https://github.com/typst/typst/issues/1982)[^1]

Long story short: I created a patch which let's you set the base folder in which typst looks for packages with an env variable and built this flake around it which does the rest of the work by pulling in the typst package sources and compiling the local project.

# TODO

- [ ] Create a 'proper' dev shell which uses the patched typst version to prevent mismatches between what is produced with `typst` and what's produced with `nix`.

[^1]: There is the _"local"_ namespace but that's also not really what I want because that basically means I have to manage the packages manually.
