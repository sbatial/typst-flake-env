#import "@preview/commute:0.1.0": arr, node, commutative-diagram
  #align(center, commutative-diagram(
    node((0, 0), [$V$]),
    node((0, 1), [$W$]),
    node((1, 0), [$K^n$]),
    node((1, 1), [$K^m$]),
    arr((0, 0), (0, 1), [$phi$], label_pos: -1em),
    arr((0, 1), (1, 1), [$D_C$], label_pos: -1.2em),
    arr((0, 0), (1, 0), [$D_B$]),
    arr((1, 0), (1, 1), [$x |-> A x$]),
  )) 
test
